php_image = registry.gitlab.com/dkx/docker/php/cli:7.4.2
php_rr_image = registry.gitlab.com/dkx/docker/php/rr:7.4.2
local_network = 127.0.0.1

.PHONY: run
run:
	docker-compose up

.PHONY: halt
halt:
	docker-compose down -v

.PHONY: deps-install
deps-install:
	docker run --rm -v `pwd`:/app $(php_image) composer install

.PHONY: parallel-lint
parallel-lint:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phplint

.PHONY: phpstan
phpstan:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpstan analyze --no-progress --no-ansi --memory-limit 1G

.PHONY: cs
cs:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpcs

.PHONY: cs-fix
cs-fix:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpcbf

.PHONY: metrics
metrics:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phpmetrics.phar --report-html=phpmetrics --exclude=vendor .

.PHONY: loc
phploc:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phploc.phar src tests

.PHONY: psalm
psalm:
	docker run --rm -v `pwd`:/app $(php_image) php tools/psalm.phar

.PHONY: test
test:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phpunit.phar

infection:
	docker run --rm -v `pwd`:/app -e PHP_XDEBUG=1 $(php_image) php tools/infection.phar --threads=4 --no-progress

test-docker:
	cp secrets/gcp/fake.json secrets/gcp/credentials.json
	docker run -d --name app-test -v `pwd`:/app -p 80:80 -p 8080:8080 --env-file env/test.env $(php_rr_image)
	sleep 5
	docker logs app-test
	curl --fail http://$(local_network):8080
	curl --fail http://$(local_network):80
	docker rm -f app-test
