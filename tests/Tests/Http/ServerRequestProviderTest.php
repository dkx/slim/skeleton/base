<?php

declare(strict_types=1);

namespace SkeletonTests\Tests\Http;

use Mockery;
use Psr\Http\Message\ServerRequestInterface;
use Skeleton\Http\ServerRequestProvider;
use SkeletonTests\TestCase;

final class ServerRequestProviderTest extends TestCase
{
	public function testGetterAndSetter() : void
	{
		$provider = new ServerRequestProvider();
		self::assertNull($provider->getRequest());
		$provider->setRequest(Mockery::mock(ServerRequestInterface::class));
		self::assertInstanceOf(ServerRequestInterface::class, $provider->getRequest());
		$provider->reset();
		self::assertNull($provider->getRequest());
	}
}
