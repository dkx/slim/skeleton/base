<?php

declare(strict_types=1);

namespace Skeleton\App;

use DKX\GoogleTracer\Psr15\TracerControllerSpanDynamicMiddleware;
use DKX\SlimNette\Configuration\ApplicationConfiguratorInterface;
use Slim\App;
use Slim\Middleware\BodyParsingMiddleware;

final class MiddlewareConfiguration implements ApplicationConfiguratorInterface
{
	private bool $debugMode;

	private ErrorHandler $errorHandler;

	public function __construct(bool $debugMode, ErrorHandler $errorHandler)
	{
		$this->debugMode    = $debugMode;
		$this->errorHandler = $errorHandler;
	}

	public function configure(App $app) : void
	{
		$app->add(TracerControllerSpanDynamicMiddleware::class);
		$app->addRoutingMiddleware();
		$app->add(BodyParsingMiddleware::class);

		$errorHandler = $app->addErrorMiddleware($this->debugMode, $this->debugMode, true);
		$errorHandler->setDefaultErrorHandler($this->errorHandler);
	}
}
