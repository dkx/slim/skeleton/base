<?php

declare(strict_types=1);

namespace Skeleton\Exception;

final class InvalidArgumentException extends \InvalidArgumentException
{
}
