<?php

declare(strict_types=1);

namespace Skeleton\Routing\System\Controllers;

use Nette\Utils\Json;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class HomeController
{
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
	{
		$response->getBody()->write(Json::encode([]));

		return $response;
	}
}
