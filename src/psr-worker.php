<?php

declare(strict_types=1);

namespace Skeleton;

use DKX\GoogleTracer\Psr15\TracerControllerSpanDynamicMiddleware;
use DKX\GoogleTracer\Tracer;
use Skeleton\App\Bootstrap;
use Skeleton\Http\ServerRequestProviderInterface;
use Slim\App;
use Spiral\Goridge\StreamRelay;
use Spiral\RoadRunner\PSR7Client;
use Spiral\RoadRunner\Worker;
use Throwable;
use function assert;
use const STDIN;
use const STDOUT;

require_once __DIR__ . '/autoload.php';

$relay  = new StreamRelay(STDIN, STDOUT);
$worker = new Worker($relay);
$psr7   = new PSR7Client($worker);

$configurator = Bootstrap::bootWithEnvProjectInfo();
$container    = $configurator->createContainer();
$isDev        = $configurator->isDebugMode();

$app = $container->getByType(App::class);
assert($app instanceof App);

$requestProvider = $container->getByType(ServerRequestProviderInterface::class);
assert($requestProvider instanceof ServerRequestProviderInterface);

$tracer = $container->getByType(Tracer::class);
assert($tracer instanceof Tracer);

$used = false;

while ($request = $psr7->acceptRequest()) {
	if ($isDev && $used) {
		$worker->stop();

		return;
	}

	$trace   = $tracer->start();
	$request = $request->withAttribute(TracerControllerSpanDynamicMiddleware::REQUEST_SPAN_ATTRIBUTE_NAME, $trace);
	$requestProvider->setRequest($request);

	try {
		$response = $app->handle($request);
		$psr7->respond($response);
		$tracer->finishSuccessfully($trace, $request, $response);
	} catch (Throwable $e) {
		$worker->error((string) $e);
		$tracer->finishWithError($trace, $request, $e);
	} finally {
		$used = true;
	}

	$requestProvider->reset();
}
