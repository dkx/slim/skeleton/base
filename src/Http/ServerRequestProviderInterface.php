<?php

declare(strict_types=1);

namespace Skeleton\Http;

use Psr\Http\Message\ServerRequestInterface;

interface ServerRequestProviderInterface
{
	public function reset() : void;

	public function getRequest() : ?ServerRequestInterface;

	public function setRequest(ServerRequestInterface $request) : void;
}
